#include <utility>

template<typename T>
class SharedPtr {
    T* ptr = nullptr;
    int* cnt = nullptr;

public:
    SharedPtr(void) noexcept {
    }
    SharedPtr(T* other) noexcept : ptr(other) {
        if (ptr) {
            cnt = new int(1);
        }
    }
    SharedPtr(const SharedPtr<T>& other) noexcept : ptr(other.ptr) {
        cnt = other.cnt;
        if (cnt) {
            (*cnt)++;
        }
    }
    SharedPtr(SharedPtr && other) noexcept : ptr(other.ptr) {
        cnt = other.cnt;
        other.ptr = nullptr;
        other.cnt = nullptr;
    }
    SharedPtr<T>& operator=(T* other) {
        if (cnt) {
            (*cnt)--;
            if (*cnt == 0) {
                delete ptr;
                delete cnt;
            }
        }
        ptr = other;
        if (ptr) {
            cnt = new int(1);
        } else {
            cnt = nullptr;
        }
        return *this;
    }
    SharedPtr<T>& operator=(const SharedPtr<T>& other) noexcept {
        if (&other == this) {
            return *this;
        }
        if (cnt) {
            (*cnt)--;
            if (*cnt == 0) {
                delete ptr;
                delete cnt;
            }
        }
        ptr = other.ptr;
        cnt = other.cnt;
        if (cnt) {
            (*cnt)++;
        }
        return *this;
    }
    SharedPtr<T>& operator=(SharedPtr<T> && other) noexcept {
        if (&other == this) {
            return *this;
        }
        if (cnt) {
            (*cnt)--;
            if (*cnt == 0) {
                delete ptr;
                delete cnt;
            }
        }
        ptr = other.ptr;
        cnt = other.cnt;
        other.ptr = nullptr;
        other.cnt = nullptr;
        return *this;
    }
    const T& operator*(void) const noexcept {
        return *ptr;
    }
    T& operator*(void) noexcept {
        return *ptr;
    }
    T* operator->(void) const noexcept {
        return ptr;
    }
    void reset(T* other) noexcept {
        if (cnt) {
            (*cnt)--;
            if (*cnt == 0) {
                delete ptr;
                delete cnt;
            }
        }
        ptr = other;
        if (ptr) {
            cnt = new int(1);
        } else {
            cnt = nullptr;
        }
    }
    void swap(SharedPtr<T>& other) noexcept {
        T* temp = other.ptr;
        int* tc = other.cnt;
        other.ptr = ptr;
        other.cnt = cnt;
        cnt = tc;
        ptr = temp;
    }
    T* get(void) const noexcept {
        return ptr;
    }
    ~SharedPtr(void) {
        if (cnt) {
            (*cnt)--;
            if (*cnt == 0) {
                delete ptr;
                delete cnt;
            }
        }
    }
    explicit operator bool() const noexcept {
        return (ptr != nullptr);
    }
};
