#include <iostream>

template<typename T>
class Vector {
    size_t sz;
    size_t cap;
    T* data;

public:
    Vector(void) {
        cap = 0;
        sz = 0;
        data = nullptr;
    }
    Vector(const Vector& other) {
        sz = other.sz;
        cap = other.cap;
        data = static_cast<T*>(operator new(sizeof(T) * cap));
        for (size_t i = 0; i < sz; i++) {
            new (data + i) T(other.data[i]);
        }
    }
    Vector& operator=(const Vector& other) {
        if (&other == this) {
            return *this;
        }
        try {
            T* next = static_cast<T*>(operator new(sizeof(T) * cap));
            for (size_t i = 0; i < cap; i++) {
                new (next + i) T(other.data[i]);
            }
            for (size_t i = 0; i < sz; i++) {
                data[i].~T();
            }
            data = next;
            cap = other.sz;
            operator delete(data);
            sz = cap;
        } catch(...) {}
        return *this;
    }
    void push_back(const T& elem) {
        if (sz < cap) {
            new (data + (sz++)) T(elem);
        } else {
            try {
                size_t new_cap = (cap == 0 ? 1 : 2 * cap);
                T* next = static_cast<T*>(operator new(sizeof(T) * new_cap));
                for (size_t i = 0; i < sz; i++) {
                    new (next + i) T(data[i]);
                }
                for (size_t i = 0; i < sz; i++) {
                    data[i].~T();
                }
                operator delete(data);
                data = next;
                new (data + (sz++)) T(elem);
                cap = new_cap;
            } catch(...) {}
        }
    }
    void push_back(T && elem) {
        if (sz < cap) {
            new (data + (sz++)) T(std::move(elem));
        } else {
            try {
                size_t new_cap = (cap == 0 ? 1 : 2 * cap);
                T* next = static_cast<T*>(operator new(sizeof(T) * new_cap));
                for (size_t i = 0; i < sz; i++) {
                    new (next + i) T(data[i]);
                }
                for (size_t i = 0; i < sz; i++) {
                    data[i].~T();
                }
                operator delete(data);
                data = next;
                new (data + (sz++)) T(std::move(elem));
                cap = new_cap;
            } catch(...) {}
        }
    }
    void pop_back(void) {
        if (sz == 0) {
            return;
        }
        data[sz--].~T();
    }
    void reserve(size_t new_cap) {
        if (new_cap <= cap) {
            return;
        }
        try {
            T* next = static_cast<T*>(operator new(sizeof(T) * new_cap));
            for (size_t i = 0; i < sz; i++) {
                new (next + i) T(data[i]);
            }
            for (size_t i = 0; i < sz; i++) {
                data[i].~T();
            }
            operator delete(data);
            data = next;
            cap = new_cap;
        } catch (...) {}
    }
    // void resize(size_t new_sz, const T& val) {
    //     if (new_sz <= sz) {
    //         for (size_t i = new_sz; i < sz; i++) {
    //             data[i].~T();
    //         }
    //         sz = new_sz;
    //         return;
    //     }
    //     reserve(new_sz);
    //     for (size_t i = sz; i < new_sz; i++) {
    //         new (data + i) T(val);
    //     }
    //     sz = new_sz;
    // }
    void resize(size_t new_sz) {
        if (new_sz <= sz) {
            for (size_t i = new_sz; i < sz; i++) {
                data[i].~T();
            }
            sz = new_sz;
            return;
        }
        try {
            if (new_sz <= cap) {
                for (size_t i = sz; i < new_sz; i++) {
                    new (data + i) T();
                }
                sz = new_sz;
            } else {
                T* next = static_cast<T*>(operator new(sizeof(T) * new_sz));
                for (size_t i = 0; i < sz; i++) {
                    new (next + i) T(data[i]);
                }
                for (size_t i = sz; i < new_sz; i++) {
                    new (next + i) T();
                }
                for (size_t i = 0; i < sz; i++) {
                    data[i].~T();
                }
                operator delete(data);
                data = next;
                sz = new_sz;
                cap = new_sz;
            }
        } catch(...) {}
    }
    void clear(void) {
        for (size_t i = 0; i < sz; i++) {
            data[i].~T();
        }
        sz = 0;
    }
    void swap(Vector& other) {
        T* t_data = other.data;
        size_t t_sz = other.sz;
        size_t t_cap = other.cap;
        other.sz = sz;
        other.cap = cap;
        data = t_data;
        sz = t_sz;
        cap = t_cap;
    }
    size_t size(void) const {
        return sz;
    }
    size_t capacity(void) const {
        return cap;
    }
    const T& operator[](size_t i) const {
        return data[i];
    }
    T& operator[](size_t i) {
        return data[i];
    }
    T* begin() {
        return data;
    }
    T* end() {
        return data + sz;
    }
    ~Vector(void) {
        for (size_t i = 0; i < sz; i++) {
            data[i].~T();
        }
        operator delete(data);
    }
};

